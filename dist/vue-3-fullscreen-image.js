import { defineComponent as O, useCssVars as D, ref as y, computed as _, onMounted as L, openBlock as s, createBlock as b, Transition as j, withCtx as A, createElementBlock as i, createElementVNode as t, createCommentVNode as v, TransitionGroup as R, normalizeClass as B, Fragment as U, renderList as P, pushScopeId as N, popScopeId as S, createApp as T, h as H } from "vue";
const h = (o) => (N("data-v-93357dc8"), o = o(), S(), o), V = { class: "icons" }, M = /* @__PURE__ */ h(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("path", { d: "M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4" }),
  /* @__PURE__ */ t("polyline", { points: "17 8 12 3 7 8" }),
  /* @__PURE__ */ t("line", {
    x1: "12",
    y1: "3",
    x2: "12",
    y2: "15"
  })
], -1)), q = [
  M
], z = /* @__PURE__ */ h(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("line", {
    x1: "18",
    y1: "6",
    x2: "6",
    y2: "18"
  }),
  /* @__PURE__ */ t("line", {
    x1: "6",
    y1: "6",
    x2: "18",
    y2: "18"
  })
], -1)), G = [
  z
], K = ["disabled"], W = /* @__PURE__ */ h(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("polyline", { points: "15 18 9 12 15 6" })
], -1)), J = [
  W
], Q = ["src", "alt"], X = ["disabled"], Y = /* @__PURE__ */ h(() => /* @__PURE__ */ t("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24",
  fill: "none",
  stroke: "currentColor",
  "stroke-width": "2",
  "stroke-linecap": "round",
  "stroke-linejoin": "round"
}, [
  /* @__PURE__ */ t("polyline", { points: "9 6 15 12 9 18" })
], -1)), Z = [
  Y
], $ = {
  key: 1,
  class: "image-container"
}, ee = ["src", "alt"], oe = /* @__PURE__ */ O({
  __name: "FullscreenImage",
  props: {
    imageUrl: {},
    anchor: {},
    animation: { default: "fade" },
    imageAlt: { default: "" },
    withDownload: { type: Boolean, default: !0 },
    withClose: { type: Boolean, default: !0 },
    withFocusOnClose: { type: Boolean, default: !0 },
    withCloseOnEscape: { type: Boolean, default: !0 },
    closeOnClikOutside: { type: Boolean, default: !0 },
    maxHeight: { default: "80vh" },
    maxWidth: { default: "80vw" },
    backdropColor: { default: "rgba(0, 0, 0, 0.7)" }
  },
  emits: ["close"],
  setup(o, { emit: c }) {
    D((e) => ({
      "08ac7e39": e.backdropColor,
      "06c3e192": e.maxHeight
    }));
    const l = o, u = c, n = y(0), a = y(!1), d = () => {
      a.value = !0, setTimeout(() => {
        u("close");
      }, 500);
    }, m = async (e) => {
      e.preventDefault(), e.stopPropagation();
      try {
        const p = Array.isArray(l.imageUrl) ? l.imageUrl[n.value] : l.imageUrl, g = await (await fetch(p)).blob(), r = document.createElement("a");
        r.href = window.URL.createObjectURL(g), r.download = "image", r.style.display = "none", document.body.appendChild(r), r.click(), document.body.removeChild(r), window.URL.revokeObjectURL(r.href);
      } catch (p) {
        console.error("Error downloading image:", p);
      }
    }, x = (e) => {
      l.withCloseOnEscape && e.key === "Escape" && d();
    }, I = () => {
      l.closeOnClikOutside && d();
    }, f = _(() => n.value === 0), C = _(() => n.value === l.imageUrl.length - 1), E = () => {
      n.value--;
    }, F = () => {
      n.value++;
    }, w = y();
    return L(() => {
      l.withFocusOnClose && w.value && w.value.focus();
    }), (e, p) => (s(), b(j, {
      name: e.animation,
      appear: ""
    }, {
      default: A(() => [
        e.imageUrl && !a.value ? (s(), i("div", {
          key: 0,
          class: "fullscreen-image",
          onKeydown: x,
          tabindex: "0",
          role: "dialog",
          "aria-modal": "true",
          "aria-label": "Fullscreen Image"
        }, [
          t("div", {
            class: "backdrop",
            onClick: I
          }, [
            t("div", V, [
              e.withDownload ? (s(), i("button", {
                key: 0,
                tabindex: "0",
                class: "icon download-icon",
                onClick: m,
                title: "close"
              }, q)) : v("", !0),
              e.withClose ? (s(), i("button", {
                key: 1,
                ref_key: "closeButtonRef",
                ref: w,
                tabindex: "0",
                class: "icon close-icon",
                onClick: d,
                title: "download"
              }, G, 512)) : v("", !0)
            ])
          ]),
          Array.isArray(e.imageUrl) ? (s(), b(R, {
            key: 0,
            name: "list",
            tag: "div",
            class: "image-container"
          }, {
            default: A(() => [
              t("button", {
                key: "previouus",
                disabled: f.value,
                class: B(["icon", f.value && "icon--disabled"]),
                onClick: E
              }, J, 10, K),
              (s(!0), i(U, null, P(e.imageUrl, (k, g) => (s(), i(U, {
                key: k + g
              }, [
                n.value === g ? (s(), i("img", {
                  key: 0,
                  src: k,
                  alt: Array.isArray(e.imageAlt) ? e.imageAlt[n.value] : e.imageAlt
                }, null, 8, Q)) : v("", !0)
              ], 64))), 128)),
              t("button", {
                key: "neeext",
                disabled: C.value,
                class: B(["icon", C.value && "icon--disabled"]),
                onClick: F
              }, Z, 10, X)
            ]),
            _: 1
          })) : (s(), i("div", $, [
            t("img", {
              src: e.imageUrl,
              alt: Array.isArray(e.imageAlt) ? e.imageAlt[n.value] : e.imageAlt
            }, null, 8, ee)
          ]))
        ], 32)) : v("", !0)
      ]),
      _: 1
    }, 8, ["name"]));
  }
}), te = (o, c) => {
  const l = o.__vccOpts || o;
  for (const [u, n] of c)
    l[u] = n;
  return l;
}, ne = /* @__PURE__ */ te(oe, [["__scopeId", "data-v-93357dc8"]]), le = {
  mounted(o, c) {
    const l = () => {
      var m;
      const n = T({
        render() {
          return H(ne, { ...c.value, onClose: () => u(n, a) });
        }
      }), a = document.createElement("div"), d = document.querySelector(((m = c.value) == null ? void 0 : m.anchor) || "body");
      d && (d.appendChild(a), n.mount(a));
    }, u = (n, a) => {
      n.unmount(), a.remove();
    };
    o.style.cursor = "pointer", o.addEventListener("click", l), o.openFullscreenImage = l;
  },
  beforeUnmount(o) {
    o.removeEventListener("click", o.openFullscreenImage);
  }
};
function ae(o) {
  o.directive("fullscreen-image", le);
}
export {
  ae as fullscreenImagePlugin
};
